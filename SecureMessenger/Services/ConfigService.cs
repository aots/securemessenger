﻿using System.Collections.Generic;

namespace SecureMessenger.Services
{
    public class ConfigService
    {
        private static ConfigService _instance;

        public static ConfigService Instance()
        {
            if(_instance == null)
                _instance = new ConfigService();
            return _instance;
        }

        private ConfigService()
        {
            dict = new Dictionary<string, object>();
            SetConfig("Nick", "NoNickName");
        }

        private Dictionary<string, object> dict;

        public T GetConfig<T>(string key)
        {
            var res = default(T);
            var @try = dict.TryGetValue(key, out object value);
            if (!@try || value == null)
                return res;
            else
                return (T) value;
        }

        public void SetConfig<T>(string key, T obj)
        {
            if (dict.ContainsKey(key))
                dict.Remove(key);
            dict.Add(key, obj);
        }
    }
}