﻿using System;
using LiteNetLib;
using LiteNetLib.Utils;
using SecureMessenger.Properties;
using Timer = System.Timers.Timer;

namespace SecureMessenger.Services
{
    public class NetService
    {
        private static NetService _instance;
        private EventBasedNetListener Listener { get; set; }
        private NetManager Client { get; set; }
        private NetDataWriter Writer { get; set; }
        private Timer Timer { get; set; }
        public static NetService Instance()
        {
            if(_instance == null)
                _instance = new NetService();
            return _instance;
        }

        private NetService()
        {
            Writer = new NetDataWriter();
            Listener = new EventBasedNetListener();
            Client = new NetManager(Listener, "SecureChat") {MaxConnectAttempts = 1};
            Timer = new Timer(100);
            Timer.Elapsed += (sender, args) => { Client.PollEvents(); };
            Timer.AutoReset = true;
            Timer.Enabled = true;
            Client.Start();
            Listener.PeerConnectedEvent += ListenerOnPeerConnectedEvent;
            Listener.NetworkErrorEvent += ListenerOnNetworkErrorEvent;
            Listener.PeerDisconnectedEvent += ListenerOnPeerDisconnectedEvent;
            Listener.NetworkReceiveEvent += ListenerOnNetworkReceiveEvent;
        }

        public void Connect()
        {
            Client.Connect(Settings.Default.Server, Settings.Default.Port);
        }

        public void Disconnect()
        {
            Client.DisconnectPeer(Client.GetFirstPeer());
        }

        #region Events

        public EventHandler<NetDataReader> RecvEvent;

        private void ListenerOnNetworkReceiveEvent(NetPeer peer, NetDataReader reader)
        {
            RecvEvent?.Invoke(this, reader);
        }

        public EventHandler<DisconnectInfo> DisconnectEvent;

        private void ListenerOnPeerDisconnectedEvent(NetPeer peer, DisconnectInfo disconnectinfo)
        {
            DisconnectEvent?.Invoke(this, disconnectinfo);
        }

        public EventHandler<int> ErrorEvent;

        private void ListenerOnNetworkErrorEvent(NetEndPoint endpoint, int socketerrorcode)
        {
            ErrorEvent?.Invoke(this, socketerrorcode);
        }

        public EventHandler<NetPeer> ConnectedEvent;

        private void ListenerOnPeerConnectedEvent(NetPeer peer)
        {
            ConnectedEvent?.Invoke(this, peer);
        }

        #endregion

        public void SendMessage(string message)
        {
            Writer.Put(string.Copy(message));
            Client.GetFirstPeer().Send(Writer, SendOptions.ReliableOrdered);
            Client.Flush();
            Writer.Reset();
        }

    }
}
