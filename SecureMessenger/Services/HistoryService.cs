﻿using System.Collections.Generic;
using System.Windows.Documents;

namespace SecureMessenger.Services
{
    public class HistoryService
    {
        private static HistoryService _instance;

        public static HistoryService Instance()
        {
            if(_instance == null)
                _instance = new HistoryService();
            return _instance;
        }

        private HistoryService()
        {

        }

        private List<string> History { get; set; }
    }
}
