﻿using System;
using System.Windows;
using AutoUpdaterDotNET;
using MahApps.Metro.Controls.Dialogs;
using SecureMessenger.Properties;

namespace SecureMessenger.Dialogs
{
    /// <summary>
    /// Interaction logic for SettingsDialog.xaml
    /// </summary>
    public partial class SettingsDialog : BaseMetroDialog
    {
        public SettingsDialog()
        {
            InitializeComponent();
            DoStartupUpdate.IsChecked = Settings.Default.StartupUpdateCheck;
            Do15Update.IsChecked = Settings.Default.TimerUpdateCheck;
            ServerIP.Text = Settings.Default.Server;
            ServerPort.Text = Settings.Default.Port.ToString();
            CancelButton.Click += CancelButtonOnClick;
            OKButton.Click += OkButtonOnClick;
            ResetButton.Click += ResetButtonOnClick;
        }

        private void ResetButtonOnClick(object sender, RoutedEventArgs e)
        {
            DoStartupUpdate.IsChecked = true;
            Do15Update.IsChecked = true;
            ServerIP.Text = "37.187.105.45";
            ServerPort.Text = "1337";
        }

        internal EventHandler CloseDialog;

        private void OkButtonOnClick(object sender, RoutedEventArgs e)
        {
            Settings.Default.Port = int.Parse(ServerPort.Text);
            Settings.Default.Server = ServerIP.Text;
            Settings.Default.StartupUpdateCheck = DoStartupUpdate.IsChecked ?? true;
            Settings.Default.TimerUpdateCheck = Do15Update.IsChecked ?? true;
            CloseDialog.Invoke(this, null);
        }

        private void CancelButtonOnClick(object sender, RoutedEventArgs e)
        {
            CloseDialog.Invoke(this, null);
        }
    }
}
