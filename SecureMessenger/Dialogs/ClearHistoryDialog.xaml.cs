﻿using System;
using System.Runtime.InteropServices;
using System.Timers;
using MahApps.Metro.Controls.Dialogs;

namespace SecureMessenger.Dialogs
{
    /// <summary>
    /// Interaction logic for ClearHistoryDialog.xaml
    /// </summary>
    public partial class ClearHistoryDialog : BaseMetroDialog
    {
        private Timer Timer { get; set; }

        public ClearHistoryDialog()
        {
            InitializeComponent();
            Loaded += ClearHistoryDialog_Loaded;
        }

        private void ClearHistoryDialog_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            Timer = new Timer(10);
            Timer.Elapsed += TimerOnElapsed;
            Timer.AutoReset = true;
            Timer.Start();
        }

        public EventHandler HistoryCleaned;

        private void TimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                if (ProgressBar.Value < 100)
                {
                    var rand = new Random();
                    var val = rand.Next(-10,10);
                    if (val > 0)
                        ProgressBar.Value += val;
                }
                else
                {
                    Timer.Stop();
                    HistoryCleaned?.Invoke(this, null);
                }
            });
        }
    }
}
