﻿using System;
using System.Windows;
using System.Windows.Input;
using MahApps.Metro.Controls.Dialogs;
using SecureMessenger.Properties;
using SecureMessenger.Services;

namespace SecureMessenger.Dialogs
{
    /// <summary>
    /// Interaction logic for SetKeyDialog.xaml
    /// </summary>
    public partial class SetKeyDialog : BaseMetroDialog
    {
        private EncryptionService _encryptionService;
        private ConfigService _configService;
        public SetKeyDialog()
        {
            InitializeComponent();
            _encryptionService = EncryptionService.Instance();
            _configService = ConfigService.Instance();
            SetButton.IsEnabled = false;
            PasswordBox.PasswordChanged += PasswordBoxOnPasswordChanged;
            SetButton.Click += SetButtonOnClick;
            CancelButton.Click += CancelButtonOnClick;
            PreviewKeyDown += OnPreviewKeyDown;
        }

        private void OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                e.Handled = true;
                if (SetButton.IsEnabled && PasswordBox.IsFocused)
                    SetButtonOnClick(this, null);
            }
        }

        public EventHandler CancelPressed;

        private void CancelButtonOnClick(object sender, RoutedEventArgs e)
        {
            CancelPressed?.Invoke(this,null);
            PasswordBox.Password = "";
            Nick.Text = "";
        }

        public EventHandler SetPressed;

        private void SetButtonOnClick(object sender, RoutedEventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(Nick.Text))
                _configService.SetConfig("Nick", Nick.Text);
            _encryptionService.Key = PasswordBox.Password;
            SetPressed?.Invoke(this, null);
            PasswordBox.Password = "";
            Nick.Text = "";
        }

        private void PasswordBoxOnPasswordChanged(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(PasswordBox.Password))
                SetButton.IsEnabled = false;
            else
                SetButton.IsEnabled = true;
        }
    }
}
