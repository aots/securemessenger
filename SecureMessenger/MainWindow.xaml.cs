﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using MahApps.Metro.Controls.Dialogs;
using SecureMessenger.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Media;
using System.Timers;
using System.Windows.Forms;
using LiteNetLib.Utils;
using SecureMessenger.Dialogs;
using SecureMessenger.Properties;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using MouseEventArgs = System.Windows.Forms.MouseEventArgs;
using Timer = System.Timers.Timer;

namespace SecureMessenger
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private Timer _typingStatusResetTimer = new Timer{AutoReset = true, Interval = 2000.0};
        private string _key;
        private EncryptionService _encService;
        private NetService _netService;
        private ConfigService _configService;
        private Brush defaultForeground;
        private Brush defaultBorderBrush;
        private SoundPlayer notification;
        private WindowState PrevWindowState { get; set; }
        private List<string> History { get; set; }
        private NotifyIcon NotifyIcon { get; set; }

        private bool _isConnected = false;

        private SetKeyDialog SetKeyDialog { get; set; }
        private bool IsConnected
        {
            get { return _isConnected; }
            set
            {
                if (value == true)
                {
                    GroupBox.Header = "Connected";
                    GroupBox.IsEnabled = true;
                    ConnectButton.IsEnabled = true;
                    ConnectButton.Foreground = new SolidColorBrush(Colors.LightSeaGreen);
                    ConnectButton.BorderBrush = new SolidColorBrush(Colors.DarkSeaGreen);
                    ConnectButton.ToolTip = "Disconnect";
                }
                else
                {
                    GroupBox.Header = "Disconnected";
                    GroupBox.IsEnabled = false;
                    ConnectButton.IsEnabled = true;
                    ConnectButton.Foreground = defaultForeground.Clone();
                    ConnectButton.BorderBrush = defaultBorderBrush.Clone();
                    ConnectButton.ToolTip = "Connect";
                }

                _isConnected = value;
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            NotifyIcon = new NotifyIcon()
            {
                Icon = Properties.Resources.icon,
                BalloonTipIcon = ToolTipIcon.None,
                BalloonTipTitle = "Secure Messenger",
                BalloonTipText = "Messenger is still running, but you will need to re-enter key for security reasons",
                Visible = true
            };
            WindowStyle = WindowStyle.ToolWindow;
            Topmost = true;
            ShowInTaskbar = false;
            PrevWindowState = WindowState;
            NotifyIcon.MouseDoubleClick += TrayDoubleClick;
            StateChanged += OnStateChanged;
            Height = Settings.Default.WindowHeight;
            Width = Settings.Default.WindowWidth;
            notification = new SoundPlayer(Properties.Resources.notification);
            History = new List<string>();
            SetKeyDialog = new SetKeyDialog();
            SetKeyDialog.SetPressed += SetPressed;
            SetKeyDialog.CancelPressed += (sender, args) => { this.HideMetroDialogAsync(SetKeyDialog); };
            _encService = EncryptionService.Instance();
            _netService = NetService.Instance();
            _configService = ConfigService.Instance();
            PreviewKeyDown += MainWindow_PreviewKeyDown;
            MessageInput.KeyDown += MessageInputOnKeyDown;
            KeyButton.Click += KeyButtonOnClick;
            ConnectButton.Click += ConnectButtonOnClick;
            CleanButton.Click += CleanButton_Click;
            SettingsButton.Click += SettingsButton_Click;
            defaultForeground = ConnectButton.Foreground.Clone();
            defaultBorderBrush = ConnectButton.BorderBrush.Clone();
            _netService.ConnectedEvent += (sender, peer) =>
            {
                Dispatcher.Invoke(() => { IsConnected = true; });
            };
            _netService.ErrorEvent += (sender, i) =>
            {
                Dispatcher.Invoke(() =>
                {
                    IsConnected = false;
                    ConnectButton.Foreground = new SolidColorBrush(Colors.IndianRed);
                    ConnectButton.BorderBrush = new SolidColorBrush(Colors.Crimson);
                    GroupBox.Header = "Connection broken!";
                });
            };
            _netService.DisconnectEvent += (sender, info) =>
            {
                Dispatcher.Invoke(() => { IsConnected = false; });
            };
            _netService.RecvEvent += RecvEvent;
            //LostFocus += OnLostFocus;
            //GotFocus += MainWindow_GotFocus;
            Closing += OnClosing;
            _typingStatusResetTimer.Elapsed += TypingStatusResetTimerOnElapsed;
            _typingStatusResetTimer.Start();
        }

        private void TypingStatusResetTimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            Dispatcher.Invoke(() => { GroupBox.Header = "Connected"; });
        }

        private void MessageInputOnKeyDown(object sender, KeyEventArgs e)
        {
            SendTypingStatus();
        }

        private void OnLostFocus(object sender, RoutedEventArgs e)
        {
            Visibility = Visibility.Hidden;
        }

        private void MainWindow_GotFocus(object sender, RoutedEventArgs e)
        {
            Visibility = Visibility.Visible;
            NotifyIcon.Icon = Properties.Resources.icon;
            //Icon = BitmapFrame.Create(Application.GetResourceStream(new Uri("if_chat_security_103752.ico", UriKind.RelativeOrAbsolute)).Stream);
        }

        private void OnStateChanged(object sender, EventArgs e)
        {
            if (WindowState == WindowState.Minimized)
            {
                Hider.Visibility = Visibility.Visible;
                NotifyIcon.ShowBalloonTip(400);
            }
            else
            {
                if (PrevWindowState == WindowState.Minimized)
                {
                    ShowKeyDialog(false);
                }
            }
            PrevWindowState = WindowState;
        }

        private void TrayDoubleClick(object sender, MouseEventArgs e)
        {
            WindowState = WindowState.Normal;
            NotifyIcon.Icon = Properties.Resources.icon;
            Activate();
        }

        private void SetPressed(object sender, EventArgs e)
        {
            ReloadHistory();
            Hider.Visibility = Visibility.Hidden;
            this.HideMetroDialogAsync(SetKeyDialog);
        }

        private void ReloadHistory()
        {
            MessageHistory.Text = "";
            foreach (var mes in History)
            {
                DecryptMessage(string.Copy(mes));
            }
        }

        private void OnClosing(object sender, CancelEventArgs e)
        {
            NotifyIcon.Visible = false;
            Settings.Default.WindowHeight = Height;
            Settings.Default.WindowWidth = Width;
            Settings.Default.Save();
        }

        private void RecvEvent(object sender, NetDataReader e)
        {
            var mes = e.GetString();
            History.Add(mes);
            var decrypted = _encService.Decrypt(mes);
            if (string.IsNullOrEmpty(decrypted))
                return;
            if (string.Equals(decrypted, "TYPING"))
            {
                Dispatcher.Invoke(() =>
                {
                    GroupBox.Header = "Someone is typing...";
                });
            }
            else
            {
                Dispatcher.Invoke(() =>
                {
                    var autoscroll = Math.Abs(ScrollViewer.VerticalOffset - ScrollViewer.ScrollableHeight) < 1f;
                    MessageHistory.Text += $"{decrypted}\r\n";
                    if (autoscroll)
                        ScrollViewer.ScrollToBottom();
                    if (!IsActive)
                    {
                        NotifyIcon.Icon = Properties.Resources.new_messages_icon;
                        notification.Play();
                    }
                });
            }
        }

        private void DecryptMessage(string message)
        {
            var decrypted = _encService.Decrypt(message);
            if (string.IsNullOrEmpty(decrypted))
                return;
            Dispatcher.Invoke(() =>
            {
                var autoscroll = Math.Abs(ScrollViewer.VerticalOffset - ScrollViewer.ScrollableHeight) < 1f;
                MessageHistory.Text += $"{decrypted}\r\n";
                if (autoscroll)
                    ScrollViewer.ScrollToBottom();
            });
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            var settingsDialog = new SettingsDialog();
            settingsDialog.CloseDialog += (o, args) => { this.HideMetroDialogAsync(settingsDialog); };
            this.ShowMetroDialogAsync(settingsDialog);
        }

        private void CleanButton_Click(object sender, RoutedEventArgs e)
        {
            var cleardialog = new ClearHistoryDialog();
            cleardialog.HistoryCleaned += (o, args) => { this.HideMetroDialogAsync(cleardialog); };
            this.ShowMetroDialogAsync(cleardialog);
            ClearHistory();
        }

        private void ConnectButtonOnClick(object sender, RoutedEventArgs e)
        {
            if (!_encService.IsKeySet)
            {
                ShowKeyDialog();
                SetKeyDialog.SetPressed += SetPressedOnConnect;
            }
            else
            {
                ConnectToggle();
            }
        }

        private void SetPressedOnConnect(object sender, EventArgs e)
        {
            ConnectToggle();
            SetKeyDialog.SetPressed -= SetPressedOnConnect;
        }

        private void ConnectToggle()
        {
            if (!IsConnected)
            {
                ConnectButton.IsEnabled = false;
                ConnectButton.Foreground = new SolidColorBrush(Colors.Goldenrod);
                ConnectButton.BorderBrush = new SolidColorBrush(Colors.DarkGoldenrod);
                ConnectButton.ToolTip = "Connecting...";
                GroupBox.Header = "Connecting to server...";
                _netService.Connect();
            }
            else
            {
                ConnectButton.IsEnabled = false;
                ConnectButton.Foreground = new SolidColorBrush(Colors.Goldenrod);
                ConnectButton.BorderBrush = new SolidColorBrush(Colors.DarkGoldenrod);
                ConnectButton.ToolTip = "Disconnecting...";
                GroupBox.Header = "Disconnecting from server...";
                _netService.Disconnect();
            }
        }

        private void KeyButtonOnClick(object sender, RoutedEventArgs e)
        {
            ShowKeyDialog();
        }

        private void MainWindow_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (Keyboard.Modifiers != ModifierKeys.Shift && e.Key == Key.Enter && MessageInput.IsFocused)
            {
                SendMessage();
                MessageInput.Text = "";
                e.Handled = true;
            }
        }

        private void SendMessage()
        {
            var nick = _configService.GetConfig<string>("Nick");
            var encmes = _encService.Encrypt($"{nick}: {MessageInput.Text}");
            _netService.SendMessage(encmes);
        }

        private void SendTypingStatus()
        {
            var encmes = _encService.Encrypt("TYPING");
            _netService.SendMessage(encmes);
        }

        private async void ShowKeyDialog(bool cancelEnabled = true)
        {
            SetKeyDialog.CancelButton.IsEnabled = cancelEnabled;
            await this.ShowMetroDialogAsync(SetKeyDialog);
        }

        private async void ShowNotImplementedMessage()
        {
            await this.ShowMessageAsync("Sorry...", "This functionality is not implemented yet.\r\nBut soon will be!");
        }

        private void ClearHistory()
        {
            History.Clear();
            MessageHistory.Text = "";
        }
    }
}
