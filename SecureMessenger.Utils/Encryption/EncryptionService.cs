﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;

namespace SecureMessenger.Utils.Encryption
{
    public class EncryptionService
    {
        private static EncryptionService _instance;

        public static EncryptionService Instance()
        {
            if (_instance == null)
                _instance = new EncryptionService();
            return _instance;
        }

        public bool IsKeySet
        {
            get;
            private set;
        }

        private byte[] KeyBytes { get; set; }
        private byte[] VectorBytes { get; set; }
        public string Key
        {
            set
            {
                var valbytes = Encoding.UTF8.GetBytes(value);
                var sha = SHA256.Create();
                KeyBytes = sha.ComputeHash(valbytes);
                VectorBytes = KeyBytes.Take(16).ToArray();
                IsKeySet = true;
            }
        }

        private SymmetricAlgorithm _algorithm;

        private EncryptionService()
        {
            _algorithm = Aes.Create();
        }

        public string Encrypt(string message)
        {
            string result = null;
            using (var encryptor = _algorithm.CreateEncryptor(KeyBytes, VectorBytes))
            {
                var buf = Encoding.UTF8.GetBytes(message);
                var sum = stringMD5(message);
                var mess = buf.Concat(sum).ToArray();
                result = Convert.ToBase64String(encryptor.TransformFinalBlock(mess, 0, mess.Length));
                Debug.WriteLine($"Encoded: {result}");
            }
            return result;
        }

        public string Decrypt(string message)
        {
            Debug.WriteLine($"Received: {message}");
            var buf = Convert.FromBase64String(message);

            Debug.WriteLine($"1: {Encoding.UTF8.GetString(buf)}");
            var str = "";
            byte[] bytes;
            using (var decryptor = _algorithm.CreateDecryptor(KeyBytes, VectorBytes))
            {

                try
                {
                    bytes = decryptor.TransformFinalBlock(buf, 0, buf.Length);
                    Debug.WriteLine($"Decoded: {Encoding.UTF8.GetString(bytes)}");
                }
                catch (DecoderFallbackException e)
                {
                    return null;
                }
                catch (CryptographicException e)
                {
                    return null;
                }
            }
            var sum = bytes.Skip(bytes.Length - 16).Take(16);
            var mess = Encoding.UTF8.GetString(bytes, 0, bytes.Length - 16);
            if (sum.SequenceEqual(stringMD5(mess)))
                return mess;
            return null;
        }

        private byte[] stringMD5(string mess)
        {
            byte[] hash;
            using (MD5 md5 = MD5.Create())
            {
                hash = md5.ComputeHash(Encoding.UTF8.GetBytes(mess));
            }

            return hash;
        }

    }
}
